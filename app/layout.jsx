import '../styles/globals.scss'
import 'normalize.css/normalize.css'
import styles from './layout.module.scss'
import Nav from './components/Nav/Nav'

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      {/*
        <head /> will contain the components returned by the nearest parent
        head.jsx. Find out more at https://beta.nextjs.org/docs/api-reference/file-conventions/head
      */}
      <head />
      <body>
        <div className={styles.wrapper}>
          <Nav></Nav>
          {children}
        </div>
        
        
      </body>
    </html>
  )
}
