
import styles from './Home.module.scss'
import Link from 'next/link'

export default function Home() {
  return (
    <main className={styles.main}>
      <h1 className={styles.title}>Combining Sass, CSS Modules with Next.js 13</h1>
      <p className={styles.tagline}>
        This is a simple example of how to configure Sass and CSS Modules with Next.js 13.
      </p>
      <code>Example code ....</code>
      <p><Link href="/">Example link...</Link></p>
    </main>
  )
}
