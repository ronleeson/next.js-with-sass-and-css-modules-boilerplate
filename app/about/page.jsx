import styles from './about.module.scss'

export default function About() {
  return (
    <main className={styles.main}>
      <h1 className={styles.aboutHeader}>About</h1>
      
      <div className={styles.sidebar}>
        <div className={styles.main}>Main content</div>
        
      </div>
    </main>
  )
}
